<?php
function ubah_huruf($string){
    $alphabet = "abcdefghijklmnopqrstuvwxyz";
    $length = strlen($string);
    $result = "";

    for($i = 0; $i < $length; $i++){
        $posisi = strpos($alphabet, $string[$i]);
        $result .= substr($alphabet, $posisi+1, 1);
    }
    return "$result <br>";
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>